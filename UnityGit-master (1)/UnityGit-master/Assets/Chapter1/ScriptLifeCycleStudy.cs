using System;
using System.Reflection;
using UnityEngine;

namespace Ramita.GameDev3.Chapter1
{
    public class ScriptLifeCycleStudy : MonoBehaviour
    {
        private int m_CallingSequence = 1;

        private void Awake()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
            
        }
        //Use this for intialization
        private void Start()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }
        //Update is called once per frame
        void Update()
        {
            
        }

        private void FixedUpdate()
        {
            
        }

        private void OnEnable()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }
        
        void OnDisable()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

        void OnDestroy()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }

        private void OnApplicationQuit()
        {
            Debug.LogFormat("{0} {1} has been called.", m_CallingSequence++,
                MethodBase.GetCurrentMethod().Name);
        }
    }
}