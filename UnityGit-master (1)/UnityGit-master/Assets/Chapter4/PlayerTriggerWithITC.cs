using UnityEngine;
using System.Diagnostics;
using Ramita.GameDev3.Chapter1;

namespace Ramita.GameDev3.Chapter4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        public GameObject player;
        private void OnTriggerEnter(Collider other)
        {
            //Get components from item object
            //Get the ItemTypeComponent component from the triggered object
            ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();

            //Get components from the player
            //Inventory
            var inventory = GetComponent<Inventory>();
            //SimpleHealthPointComponent
            var simpleHP = GetComponent<SimpleHealthPointComponent>();

            if (itc != null)
            {
                switch (itc.Type)
                {
                    case ItemType.COIN:
                        inventory.AddItem("COIN",1);
                        break;
                    case ItemType.BIGCOIN:
                        inventory.AddItem("BIGCOIN",1);
                        break;
                    case ItemType.POWERUP:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                        break;
                    case ItemType.POWERDOWN:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                        break;
                    case ItemType.BIGBODY:
                        player.transform.localScale = new Vector3(2, 2, 2);
                        break;
                    case ItemType.SMALLBODY:
                        player.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        break;
                }
            }
            
            Destroy(other.gameObject, 0);
        }
    }
}