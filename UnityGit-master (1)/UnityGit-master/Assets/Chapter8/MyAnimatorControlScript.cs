using UnityEngine;
using UnityEngine.UI;

namespace Ramita.GameDev3.Chapter8
{
    public class MyAnimatorControlScript : MonoBehaviour
    {
        protected Animator m_Animator;

        private static readonly int HP = Animator.StringToHash("HP");
        private static readonly int Dance = Animator.StringToHash("Dance");
        private static readonly int Laughing = Animator.StringToHash("Haha");
        private static readonly int Jump = Animator.StringToHash("Jump");
        private static readonly int Dying = Animator.StringToHash("Dying");

        [SerializeField] private Slider hpSlider;

        private void Start()
        {
            m_Animator = GetComponent<Animator>();

            // Initialize the slider
            hpSlider.minValue = 0f;
            hpSlider.maxValue = 100f;
            hpSlider.value = 100f;

            // Set the initial HP value in the Animator
            UpdateAnimatorHP(hpSlider.value);

            // Add a listener to update Animator when the slider value changes
            hpSlider.onValueChanged.AddListener(UpdateAnimatorHP);
        }

        private void Update()
        {
            // Update Jumping
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_Animator.SetBool(Jump, true);
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                m_Animator.SetBool(Jump, false);
            }

            // Update Laughing
            if (Input.GetKeyDown(KeyCode.L))
            {
                m_Animator.SetBool(Laughing, true);
            }
            else if (Input.GetKeyUp(KeyCode.L))
            {
                m_Animator.SetBool(Laughing, false);
            }

            // Reset Dance
            if (Input.GetKeyDown(KeyCode.R))
            {
                ReDancing();
            }

            // Debugging information
            float hpValueFromAnimator = m_Animator.GetFloat(HP);
            Debug.Log("Slider Value: " + hpSlider.value);
            Debug.Log("Animator HP Value in Update: " + hpValueFromAnimator);
        }

        public void Dancing()
        {
            m_Animator.SetTrigger(Dance);
        }

        public void ReDancing()
        {
            m_Animator.ResetTrigger(Dance);
        }

        private void UpdateAnimatorHP(float value)
        {
            Debug.Log("Updating Animator HP with value: " + value);
            m_Animator.SetFloat(HP, value);

            if (value <= 0)
            {
                m_Animator.SetBool(Dying, true);
            }
            else
            {
                m_Animator.SetBool(Dying, false);
            }
        }
    }
}
