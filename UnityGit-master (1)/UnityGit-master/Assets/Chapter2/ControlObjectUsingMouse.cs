using Unity.VisualScripting;
using UnityEngine.UI;
using UnityEngine;

namespace Ramita.GameDev3.Chapter2
{
    public class ControlObjectUsingMouse : StepMovement
    {
        private Vector3 m_MousePreviousPosiotion;
        public float m_MouseDeltaVectorScaling = 0.5f;
        private bool isClicked = false;

        void Start()
        {

        }

        void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosiotion).normalized;
            Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);

            // if (Input.GetMouseButton(0))
            // {
            //     this.transform.Translate(mouseDeltaVector*m_MouseDeltaVectorScaling,Space.World);
            // }

            if (Input.GetMouseButtonUp(0))
            {
                isClicked = false;
                Debug.Log(isClicked);
            }

            if (Input.GetMouseButtonDown(0))
            {
                isClicked = true;
                Debug.Log(isClicked);


                if (isClicked == true)
                {

                    if (mouseCurrentPos.x < 500 && (mouseCurrentPos.y > 150 && mouseCurrentPos.y < 300))
                    {
                        MoveLeft();
                    }

                    if (mouseCurrentPos.x > 500 && (mouseCurrentPos.y > 150 && mouseCurrentPos.y < 300))
                    {
                        MoveRight();
                    }

                    if (mouseCurrentPos.y > 220 && (mouseCurrentPos.x > 400 && mouseCurrentPos.x < 600))
                    {
                        MoveUp();
                    }

                    if (mouseCurrentPos.y < 220 && (mouseCurrentPos.x > 400 && mouseCurrentPos.x < 600))
                    {
                        MoveDown();
                    }

                    // if (Input.GetMouseButtonUp(0))
                    // {
                    //     this.transform.Translate(mouseDeltaVector*m_MouseDeltaVectorScaling,Space.World);
                    // }

                    this.transform.Translate(0, 0, Input.mouseScrollDelta.y * m_MouseDeltaVectorScaling, Space.World);
                    m_MousePreviousPosiotion = mouseCurrentPos;
                }
            }
        }
    }
}