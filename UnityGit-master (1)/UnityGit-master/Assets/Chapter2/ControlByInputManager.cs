using System;
using UnityEngine;

namespace Ramita.GameDev3.Chapter2
{
    public class ControlByInputManager : MonoBehaviour
    {
        public float m_MovementScale = 0.5f;

        private void Update()
        {
            float hMovement = Input.GetAxis("Horizontal") * m_MovementScale;
            float vMovement = Input.GetAxis("Vertical") * m_MovementScale;
            
            this.transform.Translate(new Vector3(hMovement,vMovement,0));

            if (Input.GetButtonDown("Fire1"))
            {
                ShootABulletInforwardDirection(Vector3.zero);
            }

            if (Input.GetButtonDown("Fire2"))
            {
                ShootABulletInforwardDirection(new Vector3(0, 1, 0), PrimitiveType.Capsule, 1.5f);
                
            }
        }

        private void ShootABulletInforwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere,
            float forceMagnitude = 1)
        {
            var newGameObject = GameObject.CreatePrimitive(type);

            newGameObject.transform.position = transform.position + transform.forward * 1.5f;
            var rigidbody = newGameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 0.15f;
            Vector3 shootingDirection = (forceModifier + transform.forward) *
                                        forceMagnitude;
            rigidbody.AddForce(shootingDirection,ForceMode.Impulse);
            Destroy(newGameObject,3);
        }
    }
}