using System;
using UnityEngine;

namespace Ramita.GameDev3.Chapter2
{
    public class ControlObjectMovementOnXZPlaneUsingWASD : StepMovement
    {
     void Update()
        {
            if(Input.GetKeyDown(KeyCode.A))
            {
                MoveLeft();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                MoveRight();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                MoveForward();
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                MoveBackward();
            }
        }
    }
}