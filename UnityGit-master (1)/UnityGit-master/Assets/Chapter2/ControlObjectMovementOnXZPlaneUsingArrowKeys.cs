using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Ramita.GameDev3.Chapter2.InputSystem
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
    {
        void Update()
        {
            // Keyboard keyboard = Keyboard.current;
            //
            //
            // if (keyboard[Key.LeftArrow].isPressed)
            // {
            //     MoveLeft();
            // }
            // else if (keyboard[Key.RightArrow].isPressed)
            // {
            //     MoveRight();
            // }
            // else if (keyboard[Key.UpArrow].isPressed)
            // {
            //     MoveForward();
            // }
            // else if (keyboard[Key.DownArrow].isPressed)
            // {
            //     MoveBackward();
            // }
            
            //GetKey
             if (Input.GetKey(KeyCode.LeftArrow))
             {
                 MoveLeft();
             }
             else if (Input.GetKey(KeyCode.RightArrow))
             {
                 MoveRight();
             }
             else if (Input.GetKey(KeyCode.UpArrow))
             {
                 MoveForward();
             }
             else if (Input.GetKey(KeyCode.DownArrow))
             {
                 MoveBackward();
             }
        }
    }
}